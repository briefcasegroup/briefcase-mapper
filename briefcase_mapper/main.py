from time import sleep
import redis
import pytesseract
import os

KEYWORDS_MAP = {
    "umowa": "Agreement",
    "zawarta": "Agreement",
    "faktura": "Invoice",
    "postanowienie": "Judicial decision",
    "wyrok": "Judicial decision",
    "potwierdzenie": "Payment confirmation",
    "apelacja": "Pleading",
    "pełnomocnictwo": "Power of attorney",
    "upoważnienie": "Power of attorney",
}


def detect_category(image_file_name):
    image_str = pytesseract.image_to_string(image_file_name, lang="pol").lower()
    for k, v in KEYWORDS_MAP.items():
        if k in image_str:
            return v
    return "Other"


def process_image(image_name: str) -> str:
    images_dir = os.getenv("IMAGES_DIRECTORY")
    return detect_category(os.path.join(images_dir, image_name))


if __name__ == "__main__":
    r = redis.Redis(host="localhost", port=6379, db=0)
    while True:
        image_data = r.lpop("image_queue")
        if image_data:
            session_id, image_name = image_data.decode("ascii").split(";")
            category = process_image(image_name)
            p = r.pipeline()
            p.hset(f"images_mappings:{session_id}", image_name, category)
            p.incr(f"processed_images:{session_id}")
            p.execute()
        else:
            sleep(1)
